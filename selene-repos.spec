%global rawhide_release 35
%global updates_testing_enabled 0

Summary:        Selene package repositories
Name:           selene-repos
Version:        1

Release:        0.0%{?eln:.eln%{eln}}
License:        MIT
URL:            https://fedoraproject.org/

Provides:       selene-repos(%{version}) = %{release}
# Need to keep the Fedora repo files here for legacy reasons.
# Once the Selene repos are a near exact mirror of the fedora repos,
# we can remove the Fedora files.
Requires:       fedora-repos
Obsoletes:      fedora-repos < 33-0.7
BuildArch:      noarch
# Required by %%check
BuildRequires:  gnupg sed

Source1:        archmap
Source9:        selene.repo

Source57:       RPM-GPG-KEY-selene

%description
Selene package repository files for yum and dnf along with gpg public keys.


%package -n selene-gpg-keys
Summary:        Selene RPM keys

%description -n selene-gpg-keys
This package provides the RPM signature keys.


# %package ostree
# Summary:        OSTree specific files

# %description ostree
# This package provides ostree specfic files like remote config from
# where client's system will pull OSTree updates.



%prep

%build

%install
# Install the keys
install -d -m 755 $RPM_BUILD_ROOT/etc/pki/rpm-gpg
install -m 644 %{_sourcedir}/RPM-GPG-KEY-selene $RPM_BUILD_ROOT/etc/pki/rpm-gpg/

# Link the primary/secondary keys to arch files, according to archmap.
# Ex: if there's a key named RPM-GPG-KEY-fedora-19-primary, and archmap
#     says "fedora-19-primary: i386 x86_64",
#     RPM-GPG-KEY-fedora-19-{i386,x86_64} will be symlinked to that key.
pushd $RPM_BUILD_ROOT/etc/pki/rpm-gpg/
# Also add a symlink for Rawhide and ELN keys
for keyfile in RPM-GPG-KEY*; do
    # resolve symlinks, so that we don't need to keep duplicate entries in archmap
    real_keyfile=$(basename $(readlink -f $keyfile))
    key=${real_keyfile#RPM-GPG-KEY-} # e.g. 'fedora-20-primary'
    if ! grep -q "^${key}:" %{_sourcedir}/archmap; then
        echo "ERROR: no archmap entry for $key"
        exit 1
    fi
    arches=$(sed -ne "s/^${key}://p" %{_sourcedir}/archmap)
    # for arch in $arches; do
        # replace last part with $arch (fedora-20-primary -> fedora-20-$arch)
        # ln -s $keyfile ${keyfile%%-*}-$arch # NOTE: RPM replaces %% with %
    # done
done
# and add symlink for compat generic location
# ln -s RPM-GPG-KEY-fedora-%{version}-primary RPM-GPG-KEY-%{version}-fedora
popd

# Install repo files
install -d -m 755 $RPM_BUILD_ROOT/etc/yum.repos.d
for file in %{_sourcedir}/selene*repo ; do
  install -m 644 $file $RPM_BUILD_ROOT/etc/yum.repos.d
done

# Enable or disable repos based on current release cycle state.
# TODO Rawhide needs to change it's name to darkside.
%if %{rawhide_release} == %{version}
rawhide_enabled=1
stable_enabled=0
testing_enabled=0
%else
rawhide_enabled=0
stable_enabled=1
testing_enabled=%{updates_testing_enabled}
%endif
for repo in $RPM_BUILD_ROOT/etc/yum.repos.d/selene*.repo; do
    sed -i "s/^enabled=AUTO_VALUE$/enabled=${stable_enabled}/" $repo || exit 1
done


# Set appropriate metadata_expire in base repo files (6h before Final, 7d after)
%if "%{release}" < "1"
expire_value='6h'
%else
expire_value='7d'
%endif
for repo in $RPM_BUILD_ROOT/etc/yum.repos.d/selene*.repo; do
    sed -i "/^metadata_expire=/ s/AUTO_VALUE/${expire_value}/" \
        $repo || exit 1
done

# I don't think OSTree is necessary for Selene Linux.  Change my mind.
# Install ostree remote config
# install -d -m 755 $RPM_BUILD_ROOT/etc/ostree/remotes.d/
# install -m 644 %{_sourcedir}/selene.conf $RPM_BUILD_ROOT/etc/ostree/remotes.d/
# install -m 644 %{_sourcedir}/selene-compose.conf $RPM_BUILD_ROOT/etc/ostree/remotes.d/


%check
# Make sure all repo variables were substituted
for repo in $RPM_BUILD_ROOT/etc/yum.repos.d/*.repo; do
    if grep -q AUTO_VALUE $repo; then
        echo "ERROR: Repo $repo contains an unsubstituted placeholder value"
        exit 1
    fi
done

# Make sure correct repos were enabled/disabled
enabled_repos=(selene)

for repo in ${enabled_repos[@]}; do
    if ! grep -q 'enabled=1' $RPM_BUILD_ROOT/etc/yum.repos.d/${repo}.repo; then
        echo "ERROR: Repo $repo should have been enabled, but it isn't"
        exit 1
    fi
done
for repo in ${disabled_repos[@]}; do
    if grep -q 'enabled=1' $RPM_BUILD_ROOT/etc/yum.repos.d/${repo}.repo; then
        echo "ERROR: Repo $repo should have been disabled, but it isn't"
        exit 1
    fi
done


# Make sure metadata_expire was correctly set
# %if "%{release}" < "1"
# expire_value='6h'
# %else
# expire_value='7d'
# %endif
# for repo in $RPM_BUILD_ROOT/etc/yum.repos.d/fedora{,-modular}.repo; do
#     lines=$(grep '^metadata_expire=' $repo | sort | uniq)
#     if [ "$(echo "$lines" | wc -l)" -ne 1 ]; then
#         echo "ERROR: Non-matching metadata_expire lines in $repo: $lines"
#         exit 1
#     fi
#     if test "$lines" != "metadata_expire=${expire_value}"; then
#         echo "ERROR: Wrong metadata_expire value in $repo: $lines"
#         exit 1
#     fi
# done

# Check arch keys exists on supported architectures
# TMPRING=$(mktemp)
# for VER in %{version} %{rawhide_release} ${rawhide_next}; do
#   echo -n > "$TMPRING"
#   for ARCH in $(sed -ne "s/^fedora-${VER}-primary://p" %{_sourcedir}/archmap)
#   do
#     gpg --no-default-keyring --keyring="$TMPRING" \
#       --import $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-fedora-$VER-$ARCH
#   done
#   # Ensure some arch key was imported
#   gpg --no-default-keyring --keyring="$TMPRING" --list-keys | grep -A 2 '^pub\s'
# done
# rm -f "$TMPRING"

%files
%dir /etc/yum.repos.d
%config(noreplace) /etc/yum.repos.d/selene.repo

%files -n selene-gpg-keys
%dir /etc/pki/rpm-gpg
/etc/pki/rpm-gpg/RPM-GPG-KEY-*


# %files ostree
# %dir /etc/ostree/remotes.d/
# /etc/ostree/remotes.d/selene.conf
# /etc/ostree/remotes.d/selene-compose.conf


%changelog
* Fri Apr 23 2021 Jason Marshall <JasonMarshallSelene@gmail.com> - 1-0.0
- Starting to build the repo files for Selene Linux
